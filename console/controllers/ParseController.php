<?php

namespace console\controllers;

use common\modules\TransactionDataMapping;
use Yii;
use common\modules\PersonLegal;
use common\modules\PersonNatural;
use yii\helpers\Console;
use yii\console\Controller;

class ParseController extends Controller
{
	const UPLOAD_DIR = '/upload';

	const PERSON_LEGAL = 'legal';
	const PERSON_NATURAL = 'natural';

	public $fileName;

	private $personLegal;
	private $personNatural;

	public function options($actionID)
	{
		return ['fileName'];
	}

	public function optionAliases()
	{
		return ['f' => 'fileName'];
	}

	public function init()
	{
		parent::init();

		$this->personLegal = new PersonLegal();
		$this->personNatural = new PersonNatural();

		Yii::setAlias('@uploadPath', Yii::getAlias('@app') . self::UPLOAD_DIR);
	}

	public function actionIndex()
	{
		$filePath = Yii::getAlias('@uploadPath') ."/$this->fileName";

		if (($handle = fopen($filePath, "r")) !== false) {
			while (($data = fgetcsv($handle, 1000, ",")) !== false) {
				$fee = 0;

				$map = $this->map($data);

				if ($map->userType === self::PERSON_LEGAL) {
					$fee = $this->personLegal->calculateFee($map);
				}

				if ($map->userType === self::PERSON_NATURAL) {
					$fee = $this->personNatural->calculateFee($map);
				}

				$this->stdout($fee, Console::BOLD);
				$this->stdout(PHP_EOL, Console::BOLD);
			}
			fclose($handle);
		}

	}

	private function map($data)
	{
		$map = new TransactionDataMapping();

		$map->date = isset($data[0]) ? $data[0] : null;
		$map->userId = isset($data[1]) ? $data[1] : null;
		$map->userType = isset($data[2]) ? $data[2] : null;
		$map->operationType = isset($data[3]) ? $data[3] : null;
		$map->amount = isset($data[4]) ? $data[4] : null;
		$map->currency = isset($data[5]) ? $data[5] : null;

		return $map;
	}
}