<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
	'currencyValues' => [
		'EUR' => '1',
		'USD' => '1.1497',
		'JPY' => '129.53'
	]
];
