<?php

namespace common\helpers;

use Yii;

class CurrencyHelper
{

	const ERROR_CURRENCY_NOT_SUPPORTED = 'Error, currency {currency} is not supported!';
	const ERROR_CURRENCY_INVALID = 'Error, currency {currency} value is invalid!';

	public function amountToCurrency($amount, $currencyFrom, $currencyTo)
	{
		$currencyParams = Yii::$app->params['currencyValues'];
		$result = new \stdClass();
		$result->amount = '';
		$result->errorMsg = false;

		if (!isset($currencyParams[$currencyFrom])) {
			$result->errorMsg = str_replace('{currency}', $currencyFrom, self::ERROR_CURRENCY_NOT_SUPPORTED);
			return $result;
		}

		if (!isset($currencyParams[$currencyTo])) {
			$result->errorMsg = str_replace('{currency}', $currencyTo, self::ERROR_CURRENCY_NOT_SUPPORTED);
			return $result;
		}

		$currencyFromValue = $currencyParams[$currencyFrom];
		$currencyToValue = $currencyParams[$currencyTo];

		if (empty($currencyFromValue) || $currencyFromValue == 0) {
			$result->errorMsg = str_replace('{currency}', $currencyFrom, self::ERROR_CURRENCY_INVALID);
			return $result;
		}

		if (empty($currencyToValue) || $currencyToValue == 0) {
			$result->errorMsg = str_replace('{currency}', $currencyTo, self::ERROR_CURRENCY_INVALID);
			return $result;
		}

		$result->amount = $amount * ($currencyToValue / $currencyFromValue);

		return $result;
	}

}