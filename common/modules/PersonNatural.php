<?php

namespace common\modules;

use common\helpers\CurrencyHelper;

class PersonNatural extends Person
{

	private $transactions = [];

	/**
	 * @param $data TransactionDataMapping
	 * @return string
	 */
	protected function calculateCashOut($data)
	{
		if (!isset($this->transactions[$data->userId])) {
			$this->transactions[$data->userId] = [];
		}

		$currencyHelper = new CurrencyHelper();

		$year = (int) date('Y', strtotime($data->date));
		$lastYear = $year - 1;

		$dayTime = 60 * 60 * 24;

		$endLastYearTime = strtotime("$lastYear-12-31");
		$dateTime = strtotime($data->date);

		$diffDays = round(($dateTime - $endLastYearTime) / $dayTime);

		$weekDay = date('w', $dateTime);
		$lastDay = date('w', $endLastYearTime);

		$weekId = $year . "-" . date('W', $dateTime);

		if ($diffDays <= 7 && $lastDay < $weekDay) {
			$weekId = $lastYear . "-" . date('W', $endLastYearTime);
		}

		$amountEur = $currencyHelper->amountToCurrency($data->amount, $data->currency, self::CURRENCY_MAIN);

		if ($amountEur->errorMsg) {
			return $amountEur->errorMsg;
		}

		$this->transactions[$data->userId][$weekId][] = $amountEur->amount;

		$sum = 0;
		$count = 0;

		$amount = 0;

		foreach ($this->transactions[$data->userId][$weekId] as $amountValue) {
			$sum += $amountValue;
			$count += 1;

			if ($count > 3) {
				$amount = $amountEur->amount;
			} elseif ($sum > 1000) {
				if (($amountEur->amount + 1000) > $sum) {
					$amount = $sum - 1000;
				} else {
					$amount = $amountEur->amount;
				}
			}
		}

		$result = $currencyHelper->amountToCurrency($amount, self::CURRENCY_MAIN, $data->currency);

		if($result->errorMsg) {
			return $result->errorMsg;
		}

		return $this->formatOutput($result->amount * self::FEE_CASH_OUT);
	}

}