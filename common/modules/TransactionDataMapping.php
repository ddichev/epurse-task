<?php

namespace common\modules;


class TransactionDataMapping
{
	public $date;
	public $userId;
	public $userType;
	public $operationType;
	public $amount;
	public $currency;
}