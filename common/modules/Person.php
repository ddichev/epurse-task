<?php

namespace common\modules;

use common\helpers\CurrencyHelper;

abstract class Person
{

	const OPERATION_TYPE_CASH_IN = 'cash_in';
	const OPERATION_TYPE_CASH_OUT = 'cash_out';
	const FEE_CASH_IN = '0.0003';
	const FEE_CASH_OUT = '0.003';
	const LIMIT_CASH_IN = '5';

	const CURRENCY_MAIN = 'EUR';

	abstract protected function calculateCashOut($data);

	/**
	 * @param $data
	 * @return string
	 */
	protected function calculateCashIn($data)
	{
		$currencyHelper = new CurrencyHelper();

		$amount = $data->amount;
		$fee = self::FEE_CASH_IN;

		$result = $currencyHelper->amountToCurrency($amount * $fee, $data->currency, $data->currency);

		$resultToEur = $currencyHelper->amountToCurrency($result->amount, $data->currency, self::CURRENCY_MAIN);

		if($resultToEur->errorMsg) {
			return $resultToEur->errorMsg;
		}

		if ($resultToEur->amount > self::LIMIT_CASH_IN) {
			$result = $currencyHelper->amountToCurrency(self::LIMIT_CASH_IN, self::CURRENCY_MAIN, $data->currency);
		}

		if($result->errorMsg) {
			return $result->errorMsg;
		}

		return $this->formatOutput($result->amount);
	}

	public function calculateFee($data)
	{

		$result = 0;

		switch ($data->operationType) {
			case self::OPERATION_TYPE_CASH_IN:
				$result = $this->calculateCashIn($data);
				break;
			case self::OPERATION_TYPE_CASH_OUT:
				$result = $this->calculateCashOut($data);
				break;
		}

		return $result;
	}

	protected function formatOutput($amount)
	{
		return number_format($amount, 2);
	}

}