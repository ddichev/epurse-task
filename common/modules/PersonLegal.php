<?php

namespace common\modules;

use common\helpers\CurrencyHelper;

class PersonLegal extends Person
{

	const LIMIT_CASH_OUT = '0.5';

	/**
	 * @param $data TransactionDataMapping
	 * @return string
	 */
	protected function calculateCashOut($data)
	{
		$currencyHelper = new CurrencyHelper();

		$amount = $data->amount;
		$fee = self::FEE_CASH_OUT;

		$result = $currencyHelper->amountToCurrency($amount * $fee, $data->currency, $data->currency);

		$resultToEur = $currencyHelper->amountToCurrency($result->amount, $data->currency, self::CURRENCY_MAIN);

		if($resultToEur->errorMsg) {
			return $resultToEur->errorMsg;
		}

		if ($resultToEur->amount < self::LIMIT_CASH_OUT) {
			$result = $currencyHelper->amountToCurrency(self::LIMIT_CASH_OUT, self::CURRENCY_MAIN, $data->currency);
		}

		if($result->errorMsg) {
			return $result->errorMsg;
		}

		return $this->formatOutput($result->amount);
	}

}