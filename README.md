Yii2 Framework is used for implementation.
CSV file is by default in epurse/console/upload dir, but this can be changed by using different script params.


Usage of application:

1. Clone repository, change to it`s local directory and do composer install.
2. For initializing framework do "php init", proceed and finish with initialization
3. For calling console controller, enter command in console:
```
php yii parse -f=input.csv
```